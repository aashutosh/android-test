package com.asterdio.newbasobaas.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asterdio.newbasobaas.CategoryDetails;
import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.R;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by User on 1/22/2018.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    private Context context;
    private List<Category> categoriesList;


    public CategoriesAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categoriesList = categories;
    }


    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_card_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final  Category model = categoriesList.get(position);
        holder.categoryTitle.setText(categoriesList.get(position).getTitle());

        Glide.with(context)
                .load(model.getImage()).into(holder.categoryImage);

        holder.categoryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cateId = categoriesList.get(position).getId();
                context.startActivity(new Intent(context, CategoryDetails.class).putExtra("ID", cateId));


            }
        });


    }

    @Override
    public int getItemCount() {

        return categoriesList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView categoryImage;
        public TextView categoryTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            categoryImage = itemView.findViewById(R.id.category_imageview);
            categoryTitle = itemView.findViewById(R.id.category_textview);
        }
    }
}
