package com.asterdio.newbasobaas.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asterdio.newbasobaas.Models.Image;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by User on 2/1/2018.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private Context context;
    private List<Property> propertyList;


    public SearchAdapter(Context context, List<Property> propertyList) {
        this.context = context;
        this.propertyList = propertyList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.property_list_card_items,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Property property = propertyList.get(position);

//

        //Property Viewholder
        holder.prop_tv_price.setText(property.getPrice());
        holder.popular_tv_location.setText(property.getLocation().getArea() + ", " + property.getLocation().getCity());

        List<Image> imageList = property.getImages();

        String thumbnail = "";
        for (Image image : imageList) {
            if (image.isThumbnail())
                thumbnail = image.getUrl();
        }
        Glide.with(context)
                .load(thumbnail)
                .into(holder.prop_image);



    }

    @Override
    public int getItemCount() {

        return propertyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView prop_image, agent_imageView, imageView_bedroom, imageView_bathroom, imageView_parking;
        public TextView prop_tv_price, popular_tv_location, textView_bathroom, textView_bedroom, textView_parking;

        public ViewHolder(View itemView) {
            super(itemView);
            prop_image = itemView.findViewById(R.id.property_imageView);
            prop_tv_price = itemView.findViewById(R.id.property_price);
            popular_tv_location = itemView.findViewById(R.id.property_location);
            agent_imageView = itemView.findViewById(R.id.property_agent_imageView);
            textView_bathroom = itemView.findViewById(R.id.property_bathroom);
            textView_bedroom = itemView.findViewById(R.id.property_bedroom);
            textView_parking = itemView.findViewById(R.id.property_parking);
        }
    }
}
