package com.asterdio.newbasobaas.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asterdio.newbasobaas.Activity.PropertyDetail;
import com.asterdio.newbasobaas.Models.Image;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 1/31/2018.
 */

public class PropertyAdapter extends RecyclerView.Adapter<PropertyAdapter.ViewHolder> {

    private List<Property> properties;
    private Context context;




    public interface OnItemClickListener {
        void onItemClicked(View v);
    }

    private OnItemClickListener listener;

    public PropertyAdapter(List<Property> properties, Context context, OnItemClickListener listener) {
        this.properties = properties;
        this.context = context;
        this.listener = listener;

    }

    @Override
    public PropertyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.property_list_card_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PropertyAdapter.ViewHolder holder, int position) {

        final Property model = properties.get(position);
        holder.price.setText("Rs. " + model.getPrice());
        holder.location.setText(model.getLocation().getCity() + " , " + model.getLocation().getArea());

        String date = model.getCreatedAt();

        if (date != null) {
            try {
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                Date new_date = format.parse(date);
                holder.date_added.setText(new SimpleDateFormat("dd MMM yyyy").format(new_date));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        holder.property_status.setText(model.getStatus());

        List<Image> imageList = model.getImages();
        String thumbnail = "";
        for (Image image : imageList) {
            if (image.isThumbnail())
                thumbnail = image.getUrl();
        }
        Glide.with(context)
                .load(thumbnail)
                .placeholder(R.drawable.placeholder)
                .into(holder.propertythumb);

        holder.propertythumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = model.getId();
                context.startActivity(new Intent(context, PropertyDetail.class).putExtra("ID", id));
            }
        });

    }


    @Override
    public int getItemCount() {
        return properties.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView price, location, bedroom, bathroom, parking, agentName, date_added, property_status, property_area;
        public ImageView propertythumb, agentImage, popupMenu;

        public ViewHolder(View itemView) {
            super(itemView);

            price = (TextView) itemView.findViewById(R.id.property_price);
            location = (TextView) itemView.findViewById(R.id.property_location);
            bedroom = (TextView) itemView.findViewById(R.id.property_bedroom);
            bathroom = (TextView) itemView.findViewById(R.id.property_bathroom);
            parking = (TextView) itemView.findViewById(R.id.property_parking);
            agentName = (TextView) itemView.findViewById(R.id.property_agent_name);
            property_area = (TextView) itemView.findViewById(R.id.property_area);
            date_added = (TextView) itemView.findViewById(R.id.property_date_added);
            property_status = (TextView) itemView.findViewById(R.id.property_status);
            propertythumb = (ImageView) itemView.findViewById(R.id.property_imageView);
            agentImage = (ImageView) itemView.findViewById(R.id.property_agent_imageView);
            popupMenu = (ImageView) itemView.findViewById(R.id.popUp_menu);

            popupMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClicked(view);
                }
            });


        }


    }
}
