package com.asterdio.newbasobaas.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asterdio.newbasobaas.Fragments.InterestedFragment;
import com.asterdio.newbasobaas.Models.Datum;
import com.asterdio.newbasobaas.Models.Image;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PropertyDetail extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap googleMap;
    private int id;
    private Session session;
    ApiInterface apiInterface;
    private Datum datum;
    private Property properties = new Property();
    private TextView description, see_more, see_less, location, price;
    public Button interesed;
    public ImageView prop_image, bathroom, bedroom, parking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        interesed = findViewById(R.id.interested_btn);
        interesed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm = getSupportFragmentManager();
                InterestedFragment interestedFragment = new InterestedFragment();
                interestedFragment.show(fm, "BuyTicket Fragment");


            }
        });

        Intent intent = getIntent();
        id = intent.getExtras().getInt("ID", id);

        prop_image = findViewById(R.id.property_detail_images);
        location = findViewById(R.id.property_detail_location);
        price = findViewById(R.id.property_detail_price);


//        agent_imageView = findViewById(R.id.property_details_agent_imageView);
//        bedroom = findViewById(R.id.property_detail_bedroom);
//        imageView_bathroom = findViewById(R.id.imageview_bathroom_property_card_items);
//        imageView_parking = findViewById(R.id.imageview_parking_property_card_items);
//        textView_bathroom = findViewById(R.id.property_bathroom);
//                bedroom = findViewById(R.id.property_bedroom);
//        textView_parking = findViewById(R.id.property_parking);


        description = findViewById(R.id.property_detail_description);
        see_more = findViewById(R.id.readMore);
        see_less = findViewById(R.id.readless);

        see_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                see_more.setVisibility(View.GONE);
                see_less.setVisibility(View.VISIBLE);
                description.setMaxLines(Integer.MAX_VALUE);
            }
        });

        see_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                see_more.setVisibility(View.VISIBLE);
                see_less.setVisibility(View.GONE);
                description.setMaxLines(2);
            }
        });


        initCollapsingToolbar();
        getPropertyDetails();


        //        Google Map Fragment

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void getPropertyDetails() {
        session = new Session(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Property> propCall = apiInterface.getPropertiesDetails(session.getJwtToken(), id);

//        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//        String jsonOutput = gson.toJson(propCall);
//        Log.e("Event::", jsonOutput);

        propCall.enqueue(new Callback<Property>() {
            @Override
            public void onResponse(Call<Property> call, Response<Property> response) {
                Property property = response.body();
//                List<Features> features = property.getFeaturesList();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String jsonOutput = gson.toJson(property);
                Log.e("DEtails::", jsonOutput);
                price.setText("Rs. " + property.getPrice());
                location.setText(property.getLocation().getCity() + " , " + property.getLocation().getArea());
                description.setText(property.getDescription());

                List<Image> imageList = property.getImages();
                String thumbnail = "";
                for (Image image : imageList) {
                    if (image.isThumbnail())
                        thumbnail = image.getUrl();
                }
                Glide.with(getApplicationContext())
                        .load(thumbnail)
                        .placeholder(R.drawable.placeholder)
                        .into(prop_image);

            }


            @Override
            public void onFailure(Call<Property> call, Throwable t) {
                Log.e("Event::", t.getMessage());

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                findViewById(R.id.collapsing_toolbar);

        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = findViewById(R.id.app_bar_layout);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;


        LatLng latLng = new LatLng(27.680472, 85.330621);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setBuildingsEnabled(true);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(27.680472, 85.330621)).zoom(18).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.property_detail_menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_to_favorite) {
            Toast.makeText(PropertyDetail.this, "Add to Favorites", Toast.LENGTH_SHORT).show();
            return true;
        } else if (id == R.id.share_menu) {
            Toast.makeText(PropertyDetail.this, "Share", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
