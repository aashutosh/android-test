package com.asterdio.newbasobaas.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asterdio.newbasobaas.Models.User;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private Button userLogin;
    ApiInterface apiInterface;
    private EditText loginEmail, loginPassword;
    private User user;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginEmail = findViewById(R.id.loginEmail);
        loginPassword = findViewById(R.id.loginPassword);

        userLogin = findViewById(R.id.loginButton);

        userLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             userLogin();

            }
        });
//        JSONObject jsonObject = new JSONObject(response.body().string());
//        JSONObject dataObject = jsonObject.getJSONObject("data");
//        String token = dataObject.getString("access_token");
//        session.setAccessToken("Bearer "+token);
//
//        String name=user.getName();
//
//
//        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        TextView register = (TextView) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });


        ImageView back = (ImageView) findViewById(R.id.back_arrow_login);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void userLogin()  {

        final User user = new User();
        session = new Session(this);
        user.setEmail(loginEmail.getText().toString().trim());
        user.setPassword(loginPassword.getText().toString().trim());
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        String ttoken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIxNzc0NTI3OTksImlhdCI6MTUxNjAyMjk5OSwiaXNzIjoiQmFzb2JhYXMgTmVwYWwiLCJuYmYiOjE1MTYwMjI5OTksImp0aSI6Ikd1ZXN0VG9rZW4iLCJzdWIiOjB9.QikmNgBYmqch5HREGFEpUs4Xk3x-zFfDg5mhYJO7jM8";
        Call<ResponseBody> call = apiInterface.logMeIn(ttoken, user);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        String token = dataObject.getString("access_token");
                        session.setJwtToken("Bearer "+token);

//                        User user1=jsonObject.getJSONObject("user");

                        Log.e("USER", jsonObject.toString());
                        Toast.makeText(LoginActivity.this, response.body().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else
                    Log.e("TestActivity", response.raw().toString());

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

}
