package com.asterdio.newbasobaas.Activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.asterdio.newbasobaas.Adapter.ViewPagerAdapter;
import com.asterdio.newbasobaas.Fragments.HotPropertyFragment;
import com.asterdio.newbasobaas.Fragments.MoreFragment;
import com.asterdio.newbasobaas.Fragments.NewPropertyFragment;
import com.asterdio.newbasobaas.Fragments.PopularPropertyFragment;
import com.asterdio.newbasobaas.R;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddPropertyActivity.class));
                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
        });
        viewPager = findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_popularcity);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_hotproperty);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_newproperty);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_more);
        viewPager.setCurrentItem(0);
        final int selectedTabColor = ContextCompat.getColor(MainActivity.this, R.color.colorPrimary);
        final int unselectedTabColor = ContextCompat.getColor(MainActivity.this, R.color.grey);
        tabLayout.getTabAt(0).getIcon().setColorFilter(selectedTabColor, PorterDuff.Mode.SRC_IN);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(selectedTabColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(unselectedTabColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PopularPropertyFragment(), "Popular");
        adapter.addFragment(new HotPropertyFragment(), "Hot");
        adapter.addFragment(new NewPropertyFragment(), "New");
        adapter.addFragment(new MoreFragment(), "More");
        viewPager.setAdapter(adapter);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

         if (id == R.id.search_menu) {
            startActivity(new Intent(MainActivity.this, SearchActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
