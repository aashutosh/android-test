package com.asterdio.newbasobaas.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asterdio.newbasobaas.Adapter.PropertyAdapter;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements PropertyAdapter.OnItemClickListener {

    private EditText search;
    public String keyword;
    private ImageView back_button;
    private Session session;

    private RecyclerView searchRecyclerView;
    private PropertyAdapter propertyAdapter;
    private PropertyAdapter.OnItemClickListener listener;

    ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.search_toolBar);
        setSupportActionBar(toolbar);

        search = findViewById(R.id.searchBar);
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });
        back_button = findViewById(R.id.back_arrow);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        searchRecyclerView = findViewById(R.id.search_result_recyclerView);
        searchRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        searchRecyclerView.setItemAnimator(new DefaultItemAnimator());
        searchRecyclerView.setHasFixedSize(true);
    }

    private void performSearch() {

        keyword = search.getText().toString();
        Toast.makeText(this, keyword, Toast.LENGTH_SHORT).show();
        session = new Session(this);
        Call<List<Property>> searchCall = apiInterface.getSearch(session.getJwtToken(), keyword);
        searchCall.enqueue(new Callback<List<Property>>() {
            @Override
            public void onResponse(Call<List<Property>> call, Response<List<Property>> response) {
                List<Property> propertyList = response.body();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String jsonOutput = gson.toJson(propertyList);
                Log.e("Event::", jsonOutput);
                propertyAdapter = new PropertyAdapter(propertyList, SearchActivity.this, listener);
                searchRecyclerView.setAdapter(propertyAdapter);
            }

            @Override
            public void onFailure(Call<List<Property>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClicked(View v) {
//        FragmentManager fm = getSupportFragmentManager();
//        PopUpMenu popUpMenu = new PopUpMenu();
//        new PopUpMenu().show(fm, "Pop UP menu");
    }
}
