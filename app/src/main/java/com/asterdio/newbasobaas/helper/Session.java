package com.asterdio.newbasobaas.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by User on 1/17/2018.
 */

public class Session {
    Context context;
    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        this.context = cntx;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setJwtToken(String token) {
        prefs.edit().putString("token", token).commit();
    }

    public String getJwtToken() {
        String token = prefs.getString("token", "");
        if (token == null || token.isEmpty()) {
            token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIxNzc0NTI3OTksImlhdCI6MTUxNjAyMjk5OSwiaXNzIjoiQmFzb2JhYXMgTmVwYWwiLCJuYmYiOjE1MTYwMjI5OTksImp0aSI6Ikd1ZXN0VG9rZW4iLCJzdWIiOjB9.QikmNgBYmqch5HREGFEpUs4Xk3x-zFfDg5mhYJO7jM8";
        }
        return token;
    }
    public void setRefToken(String token) {
        prefs.edit().putString("token", token).commit();
    }

    public String getRefToken() {
        String token = prefs.getString("token", "");
        if (token == null || token.isEmpty()) {
            token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIxNzc0NTI3OTksImlhdCI6MTUxNjAyMjk5OSwiaXNzIjoiQmFzb2JhYXMgTmVwYWwiLCJuYmYiOjE1MTYwMjI5OTksImp0aSI6Ikd1ZXN0VG9rZW4iLCJzdWIiOjB9.QikmNgBYmqch5HREGFEpUs4Xk3x-zFfDg5mhYJO7jM8";
        }
        return token;
    }
}

