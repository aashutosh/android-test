package com.asterdio.newbasobaas.rest;

import android.content.Context;
import android.util.Log;

import com.asterdio.newbasobaas.helper.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Callback;

public class ApiAuthenticator implements Authenticator {
    Context context;
    String newAccessToken="";

    public ApiAuthenticator(Context context) {
        this.context = context;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        final Session session = new Session(context);

        if (responseCount(response) >= 3) {
            Log.e("AUTHENTICATOR", "Failed 3 times returned null");

            return response.request().newBuilder()
                    .header("Authorization", session.getJwtToken())
                    .build(); // If we've failed 3 times, give up.
        } else if (response.code() == 201) {
            Log.e("AUTHENTICATOR", "AccessToken expired.");
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("refreshToken", session.getJwtToken());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ApiInterface asd = ServiceGenerator.createService(ApiInterface.class);
            final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Call<ResponseBody> newCall = asd.getNewtoken(requestBody);
            newCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    JSONObject resultObject= null;
                    try {
                        resultObject = new JSONObject(response.raw().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JSONObject tokenObject = null;
                    String newToken= null;
                    try {
                        newToken = tokenObject.getString("access_token");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    newToken="Bearer "+newToken;
                session.setJwtToken(newToken);
                Log.e("Token Response s Object", session.getJwtToken());
                Log.e("Token Response s Object", session.getRefToken());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });


        }
            return response.request().newBuilder()
                    .header("Authorization", session.getJwtToken())
                    .build();
        }




    private int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }
}