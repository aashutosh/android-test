package com.asterdio.newbasobaas.rest;

import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.Models.Datum;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.Models.User;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {


    //    @POST("/api/users/login")
//    Call<ResponseBody> loginUser(@Header("Authorization") String token, @Body User user);

    @POST("/api/users/login")
    Call<ResponseBody> logMeIn(@Header("Authorization") String token, @Body User user);

    @GET("/api/categories")
Call<List<Category>> getCategories();

    @GET("/api/properties-hot")
    Call<Datum> getHotProperties(@Header("Authorization") String token);

    @GET("/api/properties-latest")
    Call<Datum> getNewProperties(@Header("Authorization") String token);

    @GET("/api/properties-popular")
    Call<Datum> getPopularProperties(@Header("Authorization") String token);

    @GET("/api/property/{id}")
    Call<Property> getPropertiesDetails(@Header("Authorization") String accessToken, @Path("id") int id);

    @POST("/api/users/signup")
    Call<ResponseBody> signMeUp(@Header("Authorization") String token, @Body User user);

    @POST("/api/property-form")
    Call<Datum> getPropertyForm();

    @POST("/api/token/refresh")
    Call<ResponseBody> getNewtoken(@Body RequestBody requestBody);

    @GET("/api/search")
    Call<List<Property>> getSearch (@Header("Authorization") String token, @Query("q") String query);

    @GET("/api/properties/category")
    Call<List<Property>> getCategoryDetails (@Header("Authorization") String token);
//
//    @GET("/api/features")
//    Call<Features> getFeatures(@Header("Authorization") String token);


}
