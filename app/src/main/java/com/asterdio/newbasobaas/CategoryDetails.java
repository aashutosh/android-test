package com.asterdio.newbasobaas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;

import com.asterdio.newbasobaas.Adapter.PropertyAdapter;
import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryDetails extends AppCompatActivity {
    private EditText search;
    public String keyword;
    private Category category;


    private int cateId;
    private Property properList = new Property();

    private Session session;
    ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

    private RecyclerView categoryDetailsRecyclerView;
    private PropertyAdapter propertyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);
        session = new Session(CategoryDetails.this);
        categoryDetailsRecyclerView = findViewById(R.id.categoryDetails_recyclerView);
        categoryDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        categoryDetailsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryDetailsRecyclerView.setHasFixedSize(true);



        session = new Session(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<List<Property>> propCall = apiInterface.getCategoryDetails(session.getJwtToken());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(properList);
        Log.e("Event::", jsonOutput);
            propCall.enqueue(new Callback<List<Property>>() {
                @Override
                public void onResponse(Call<List<Property>> call, Response<List<Property>> response) {

                }

                @Override
                public void onFailure(Call<List<Property>> call, Throwable t) {

                }
            });
    }
}