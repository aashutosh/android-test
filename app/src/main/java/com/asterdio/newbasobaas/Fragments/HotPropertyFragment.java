package com.asterdio.newbasobaas.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asterdio.newbasobaas.Adapter.CategoriesAdapter;
import com.asterdio.newbasobaas.Adapter.PropertyAdapter;
import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.Models.Datum;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HotPropertyFragment extends Fragment implements PropertyAdapter.OnItemClickListener {

    private RecyclerView categoryRecyclerView, property_recycler;
    private CategoriesAdapter categoriesAdapter;
    private List<Category> categoriesList;

    private PropertyAdapter propertyAdapter;
    private PropertyAdapter.OnItemClickListener listener;
    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
    private Session session;

    private Datum datum;

    public HotPropertyFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hot_property, container, false);

//Category RecyclerView
        session = new Session(getContext());
        categoryRecyclerView = view.findViewById(R.id.hot_property_category_recyclerView);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryRecyclerView.setHasFixedSize(true);

        Call<List<Category>> categoriesCall = apiInterface.getCategories();
        categoriesCall.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.code() == 200) {

                    categoriesList = response.body();
                    categoriesAdapter = new CategoriesAdapter(getContext(), categoriesList);
                    categoryRecyclerView.setAdapter(categoriesAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.e("Hot Property Category", t.getMessage());
            }
        });

//        Property List RrecyclerView

        property_recycler = view.findViewById(R.id.hot_properties_recyclerview);
        property_recycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        property_recycler.setHasFixedSize(true);
        property_recycler.setItemAnimator(new DefaultItemAnimator());
        listener = this;
        Call<Datum> propertyCall = apiInterface.getHotProperties(session.getJwtToken());
        propertyCall.enqueue(new Callback<Datum>() {
            @Override
            public void onResponse(Call<Datum> call, Response<Datum> response) {
                if (response.code() == 200) {
                    datum = response.body();

                    List<Property> propertyList = datum.getPropertyList();

//                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
//                    String jsonOutput = gson.toJson(propertyList);
//                    Log.e("Property List::", jsonOutput);


                    propertyAdapter = new PropertyAdapter(propertyList, getContext(), listener);
                    property_recycler.setAdapter(propertyAdapter);
                }
            }

            @Override
            public void onFailure(Call<Datum> call, Throwable t) {
                Log.e("HotProperty Fragment", t.getMessage());
            }
        });

        return view;
    }


    @Override
    public void onItemClicked(View v) {

        FragmentManager fm = getChildFragmentManager();
        PopUpMenu popUpMenu = new PopUpMenu();
        popUpMenu.show(fm, "Pop UP menu");
    }
}
