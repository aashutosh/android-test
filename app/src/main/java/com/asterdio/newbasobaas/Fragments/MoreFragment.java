package com.asterdio.newbasobaas.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.asterdio.newbasobaas.Activity.AboutUsActivity;
import com.asterdio.newbasobaas.Activity.ContactUsActivity;
import com.asterdio.newbasobaas.Activity.LoginActivity;
import com.asterdio.newbasobaas.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment {

    private Button login_btn;
    private LinearLayout profile, contact, logout, about_us;

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);

//      Login Button
        login_btn = view.findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });

//      Profile Card
        profile = view.findViewById(R.id.profile_card);
        profile.setVisibility(View.GONE);

//      Contact Us Card
        contact = view.findViewById(R.id.contact_us_card);
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ContactUsActivity.class));
            }
        });

//      About Us Card
        about_us = view.findViewById(R.id.about_us_card);
        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AboutUsActivity.class));
            }
        });

//      LOgout Card
        logout = view.findViewById(R.id.logout_card);
        logout.setVisibility(View.GONE);
        return view;
    }


}
