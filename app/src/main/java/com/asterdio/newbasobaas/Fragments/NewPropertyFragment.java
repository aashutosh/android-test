package com.asterdio.newbasobaas.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asterdio.newbasobaas.Adapter.CategoriesAdapter;
import com.asterdio.newbasobaas.Adapter.PropertyAdapter;
import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.Models.Datum;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPropertyFragment extends Fragment implements PropertyAdapter.OnItemClickListener {
    private RecyclerView categoryRecyclerView, property_recyclerView;
    private CategoriesAdapter categoriesAdapter;
    private List<Category> categoriesList;

    private Session session;
    private PropertyAdapter propertyAdapter;
    private PropertyAdapter.OnItemClickListener listener;
    private Datum datum;

    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

    public NewPropertyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_property, container, false);

//        Category RecyclerView
        categoryRecyclerView = view.findViewById(R.id.new_property_category_recyclerView);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryRecyclerView.setHasFixedSize(true);
        session = new Session(getContext());

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Category>> categoriesCall = apiInterface.getCategories();
        categoriesCall.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.code() == 200) {
                    categoriesList = response.body();
                    categoriesAdapter = new CategoriesAdapter(getContext(), categoriesList);
                    categoryRecyclerView.setAdapter(categoriesAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.e("New Property Category", t.getMessage());
            }
        });


        //        Property List RrecyclerView

        property_recyclerView = view.findViewById(R.id.new_property_recyclerview);
        property_recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        property_recyclerView.setHasFixedSize(true);
        property_recyclerView.setItemAnimator(new DefaultItemAnimator());
        listener = this;

        Call<Datum> propertyCall = apiInterface.getNewProperties(session.getJwtToken());
        propertyCall.enqueue(new Callback<Datum>() {
            @Override
            public void onResponse(Call<Datum> call, Response<Datum> response) {
                if (response.code() == 200) {
                    datum = response.body();

                    List<Property> propertyList = datum.getPropertyList();
                    propertyAdapter = new PropertyAdapter(propertyList, getContext(), listener);
                    property_recyclerView.setAdapter(propertyAdapter);
                }
            }

            @Override
            public void onFailure(Call<Datum> call, Throwable t) {
                Log.e("New Property Fragment", t.getMessage());
            }
        });


        return view;
    }


    @Override
    public void onItemClicked(View v) {
        FragmentManager fm = getFragmentManager();
        PopUpMenu popUpMenu = new PopUpMenu();
        popUpMenu.show(fm, "Pop UP menu");
    }
}
