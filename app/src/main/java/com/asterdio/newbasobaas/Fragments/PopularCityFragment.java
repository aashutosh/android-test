package com.asterdio.newbasobaas.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asterdio.newbasobaas.Adapter.CategoriesAdapter;
import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PopularCityFragment extends Fragment {
    private RecyclerView categoryRecyclerView;
    private CategoriesAdapter categoriesAdapter;
    private List<Category> categoriesList;
    private ApiInterface apiInterface;
    private Session session;

    public PopularCityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular_city, container, false);

        categoryRecyclerView = view.findViewById(R.id.category_recyclerView);
        categoriesList = new ArrayList<>();

        categoriesAdapter = new CategoriesAdapter(getContext(), categoriesList);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryRecyclerView.setHasFixedSize(true);
        session = new Session(getContext());
        //        categoryRecyclerView.setAdapter(categoriesAdapter);


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Category>> categoriesCall = apiInterface.getCategories();
        categoriesCall.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.code() == 200) {
                    categoriesList = response.body();

                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    String jsonOutput = gson.toJson(categoriesList);
                    Log.e("Event::", jsonOutput);

                    categoriesAdapter = new CategoriesAdapter(getContext(), categoriesList);
                    categoryRecyclerView.setAdapter(categoriesAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.e("Home Fragment", t.getMessage());
            }
        });

        return view;
    }


}
