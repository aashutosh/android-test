package com.asterdio.newbasobaas.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.asterdio.newbasobaas.Adapter.CategoriesAdapter;
import com.asterdio.newbasobaas.Adapter.PropertyAdapter;
import com.asterdio.newbasobaas.Models.Category;
import com.asterdio.newbasobaas.Models.Datum;
import com.asterdio.newbasobaas.Models.Property;
import com.asterdio.newbasobaas.R;
import com.asterdio.newbasobaas.helper.Session;
import com.asterdio.newbasobaas.rest.ApiClient;
import com.asterdio.newbasobaas.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InterestedFragment extends DialogFragment {

    private RecyclerView categoryRecyclerView, property_recyclerView;
    private CategoriesAdapter categoriesAdapter;
    private List<Category> categoriesList;

    private Session session;
    private PropertyAdapter propertyAdapter;
    private PropertyAdapter.OnItemClickListener listener;
    private Datum datum;

    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
    public InterestedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_interested, container, false);
        ImageButton close_btn = (ImageButton) view.findViewById(R.id.ib_close);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .remove(InterestedFragment.this).commit();
            }
        });

        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LinearLayout root = new LinearLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);



        Call<Datum> propertyCall = apiInterface.getPropertyForm();
        propertyCall.enqueue(new Callback<Datum>() {
            @Override
            public void onResponse(Call<Datum> call, Response<Datum> response) {
                if (response.code() == 200) {
                    datum = response.body();

                    List<Property> propertyList = datum.getPropertyList();
                    propertyAdapter = new PropertyAdapter(propertyList, getContext(), listener);
                    property_recyclerView.setAdapter(propertyAdapter);
                }
            }

            @Override
            public void onFailure(Call<Datum> call, Throwable t) {
                Log.e("New Property Fragment", t.getMessage());
            }
        });

        return dialog;

    }
    }
