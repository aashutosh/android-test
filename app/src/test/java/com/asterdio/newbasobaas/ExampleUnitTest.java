package com.asterdio.newbasobaas;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <radio_btn_selected href="http://d.android.com/tools/testing">Testing documentation</radio_btn_selected>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}